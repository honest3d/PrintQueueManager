import logger from './logger.js';
import express from 'express';
import session from 'express-session';

import MongoStore from 'connect-mongo';
import mongoose from 'mongoose';


import passport from './passport/setup.js';
import auth from './routes/auth.js';

const port = 3000;
const mongo_uri = 'mongodb://server.lucasteng.ca:27017/print_queue';

const app = express();

mongoose
    .connect(mongo_uri, {useNewUrlParser: true})
    .then(logger.info(`MongoDB Connected at ${mongo_uri}`))
    .catch(err=>logger.error(err));

app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use(
    session({
        secret:'E78f-QLvw__NPnpgbH93LxLn',
        resave: false,
        saveUninitialized: true,
        store: MongoStore.create({client: mongoose.connection.getClient()})
    })
);

//passport
app.use(passport.initialize());
app.use(passport.session());


logger.info('Initializing');


app.get('/', (req, res) => res.send('Good morning sunshine!'));
app.get('/fail', (req,res)=>{
    logger.error(req);
    res.status(500).send('error authenticating');
});
app.use('/api/auth', auth);

app.listen(port, () => logger.info(
    `Example app listening on port ${port}!`
));