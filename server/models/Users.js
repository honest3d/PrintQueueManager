import mongoose from 'mongoose';

const UserSchema = new mongoose.Schema({
    name:{
        type: String
    },
    username:{
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String
    },
    discordId: {
        type: String
    },
    discordProfile: {
        type:{}
    }
},
{strict:false});
const User = mongoose.model('users', UserSchema);

export default User;