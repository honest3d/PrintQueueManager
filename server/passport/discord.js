import bcrypt from 'bcryptjs';
import passport_discord from 'passport-discord';

import dotenv from 'dotenv';
import User from '../models/Users.js';
import logger from '../logger.js';

//todo move this to index
dotenv.config();

const DiscordStrategy = passport_discord.Strategy;

var scope = ['identify', 'guilds']; 

const strategy = new DiscordStrategy({
    clientID: process.env.DISCORD_ID,
    clientSecret: process.env.DISCORD_SECRET,
    callbackURL: process.env.DISCORD_CALLBACK,
    scope
},
(accessToken,refreshToken,profile,done)=>{
    logger.info('checking discord auth');
    User.findOne({discordId:profile.id})
        .then(user=>{
            if(!user){
                let username = `${profile.username}#${profile.discriminator}`;
                let password = `${profile.username}${profile.fetchedAt}`;
                const newUser = new User({username,password,name:profile.username,discordId:profile.id,discordProfile:profile});
                bcrypt.genSalt(10,(err,salt)=>{
                    if(err) logger.error(err);
                    bcrypt.hash(newUser.password, salt, (err,hash)=>{
                        if (err) logger.error(err);
                        newUser.password = hash;
                        newUser
                            .save()
                            .then(user=>done(null,user))
                            .catch(err=> done(null,false,{message: err}));
                    });
                });
            }
        });
});

export default strategy;