import bcrypt from 'bcryptjs';
import User from '../models/Users.js';
import passport from 'passport';
import passport_local from 'passport-local'; 

import DiscordStrategy from './discord.js';

import logger from '../logger.js';

const LocalStrategy = passport_local.Strategy;

passport.serializeUser((user,done)=>{
    done(null,user.id);
});

passport.deserializeUser((id,done)=>{
    User.findById(id,(err,user)=>{
        done(err,user);
    });
});


passport.use(
    new LocalStrategy({usernameField:'username'}, (username,password,done)=>{
        User.findOne({username})
            .then(user=>{
                // if there is no user by this username
                if(!user){
                    const newUser = new User({username,password});
                    bcrypt.genSalt(10, (err, salt)=>{
                        bcrypt.hash(newUser.password, salt, (err,hash)=>{
                            if (err) logger.error(err);
                            newUser.password = hash;
                            newUser
                                .save()
                                .then(user => done(null, user))
                                .catch(err => done(null, false, {message: err}));
                        });
                    });
                // check the password
                } else {
                    bcrypt.compare(password, user.password, (err, isMatch) => {
                        if (err) logger.error(err);
                        
                        if(isMatch) return done(null, user);
                        else return done(null, false, {message:'Wrong Password'});
                    });
                }
            })
            .catch(err => done(null, false, {message: err}));
    })
);
passport.use(DiscordStrategy);

export default passport;