import express from 'express';
import passport from 'passport';
import logger from '../logger.js';



const router = express.Router();

router.post('/register_login', (req, res, next)=>{
    passport.authenticate('local', (err, user, info)=>{
        logger.info(info);
        if(err) return res.status(400).json({errors:err});
        if(!user) return res.status(400).json({errors: 'No user found'});
        req.logIn(user, errors=>{
            if (errors) return res.status(400).json({errors});
            return res.status(200).json({success: `logged in ${user.id}`});
        });
        
    })(req,res,next);
});
router.get('/discord', passport.authenticate('discord'));
router.get('/discord/callback', (req, res, next)=>{
    passport.authenticate('discord', (err,user,info)=>{
        logger.info(info);
        if(err) return res.status(400).json({errors:err});
        if(!user) return res.status(400).json({errors: 'No user found'});
        req.logIn(user, errors=>{
            if(errors) return res.status(400).json({errors});
            return res.status(200).json({success: `logged in ${user.username}`});
        });
    })(req,res,next);
});



export default router;